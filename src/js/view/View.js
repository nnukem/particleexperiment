import * as THREE from 'three';
import FBOUtils from "./../three/FBOUtils";
import Controls from "./../three/Controls";

export default class View{

  constructor(){
    this.WIDTH = window.innerWidth;
    this.HEIGHT = window.innerHeight;
    this.ASPECT = this.WIDTH / this.HEIGHT;
    this.VIEW_ANGLE = 45;
    this.NEAR = 0.1;
    this.FAR = 10000;
    this.W = 512;
    this.H = 512;
    this.PARTICLE_NUM = this.W*this.H;
    this.RADIUS = 100;

    this.conainer;
    this.renderer;
    this.camera;
    this.scene;

    this.sphere;

    this.timer = 0;

    this.init3d();
    this.initData();
    this.initFBO();
    this.initGeometry();
    this.initControls();
    this.render();
  }
  init3d() {
    var directionalLight;

    //div element that will hold renderer
    this.container = document.getElementById('view');

    //renderer
    this.renderer = new THREE.WebGLRenderer();
    this.renderer.gammaInput = true;
    this.renderer.gammaOutput = true;
    this.renderer.setSize(this.WIDTH, this.HEIGHT);
    this.renderer.setClearColor(0, 1.0);
    this.container.appendChild(this.renderer.domElement);

    //camera
    this.camera = new THREE.PerspectiveCamera(this.VIEW_ANGLE, this.ASPECT, this.NEAR, this.FAR);
    this.camera.position.z = 300;

    //lights

    this.directionalLight = new THREE.DirectionalLight(0xffffff, 1.2);
    this.directionalLight.position.set(0, -1, 0);

    this.scene = new THREE.Scene();
    this.scene.add(this.camera);
    this.scene.add(this.directionalLight);
    //scene.add(sphere);

    /**/




  }

  initData(){
    this.data = new Float32Array( this.PARTICLE_NUM*3 );
    for (var i = 0; i < this.PARTICLE_NUM; i++) {

        var ind = i*3
        var phi = THREE.Math.randFloatSpread(Math.PI*2);
        var theta = THREE.Math.randFloatSpread(Math.PI*2);
        var costheta = THREE.Math.randFloatSpread(2);
        var theta = Math.acos(costheta);

        this.data[ind] = this.RADIUS * Math.sin(theta) * Math.cos(phi);
        this.data[ind+1] = this.RADIUS * Math.sin(theta) * Math.sin(phi);
        this.data[ind+2] = this.RADIUS * Math.cos(theta);

    }
  }

  initFBO(){

    this.dataTexture = new THREE.DataTexture(this.data, this.W,this.H, THREE.RGBFormat, THREE.FloatType );
    this.dataTexture.minFilter = THREE.NearestFilter;
    this.dataTexture.magFilter = THREE.NearestFilter;
    this.dataTexture.needsUpdate = true;

    var rtTexturePos = new THREE.WebGLRenderTarget(this.W, this.H, {
      wrapS:THREE.RepeatWrapping,
      wrapT:THREE.RepeatWrapping,
      minFilter: THREE.NearestFilter,
      magFilter: THREE.NearestFilter,
      format: THREE.RGBFormat,
      type:THREE.FloatType,
      stencilBuffer: false
    });

    var rtTexturePos2 = rtTexturePos.clone();

    this.simulationShader = new THREE.ShaderMaterial({

      uniforms: {
        tPositions: { type: "t", value: this.dataTexture },
        origin: { type: "t", value: this.dataTexture },
        timer: { type: "f", value: 0},
        mousePos: { type: "v3", value: new THREE.Vector3()}
      },

      vertexShader: document.getElementById('texture_vertex_simulation_shader').textContent,
      fragmentShader:  document.getElementById('texture_fragment_simulation_shader').textContent

    });

    this.fboParticles = new FBOUtils( this.W, this.renderer, this.simulationShader );
    this.fboParticles.renderToTexture(rtTexturePos.texture, rtTexturePos2);

    this.fboParticles.in = rtTexturePos;
    this.fboParticles.out = rtTexturePos2;

    this.material = new THREE.ShaderMaterial( {

      uniforms: {

        "map": { type: "t", value: rtTexturePos.texture },
        "width": { type: "f", value: this.W },
        "height": { type: "f", value: this.H },
        "mousePos": { type: "v3", value: new THREE.Vector3()},
        "pointSize": { type: "f", value: 2 }

      },
      vertexShader: document.getElementById( 'vs-particles' ).textContent,
      fragmentShader: document.getElementById( 'fs-particles' ).textContent,
      depthTest: false,
      transparent: true

    } );
  }

  initGeometry(){
    var geometry = new THREE.Geometry();

    for (var i = 0; i < this.PARTICLE_NUM; i++) {
        var ind = i*3
        var vertex = new THREE.Vector3();

        vertex.x = this.data[ind]
        vertex.y = this.data[ind+1]
        vertex.z = this.data[ind+2]

        geometry.vertices.push(vertex);
    }

    this.particles = new THREE.Points(geometry, this.material);
    // this.particles.boundingSphere = 50;
    this.scene.add(this.particles);
  }

  initControls(){
    this.controls = new Controls(this.particles,this.renderer.domElement,this.camera)
    this.controls.enabled = true;
    this.controls.minPitchAngle = -Math.PI/3
    this.controls.maxPitchAngle = Math.PI/3
    this.controls.enableDamping = true
    this.controls.dampingFactor = 0.15
    this.controls.rotateSpeed =0.25
    this.controls.addEventListener("select",this.selectHandler.bind(this))
  }

  render() {
      requestAnimationFrame(this.render.bind(this));
      this.timer += 0.01;

      this.simulationShader.uniforms.timer.value = this.timer;

      // swap
      var tmp = this.fboParticles.in;
      this.fboParticles.in = this.fboParticles.out;
      this.fboParticles.out = tmp;

      this.simulationShader.uniforms.tPositions.value = this.fboParticles.in.texture;
      this.fboParticles.simulate(this.fboParticles.out);
      this.material.uniforms.map.value = this.fboParticles.out.texture;

      this.renderer.render(this.scene, this.camera);

  }


  selectHandler(e){
    console.log(this.simulationShader.uniforms);
    this.simulationShader.uniforms.mousePos.value = e.point;
  }
}
