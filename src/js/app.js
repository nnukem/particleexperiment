import $ from 'jquery'
import View from "./view/View"

export default class App{
  constructor(){
    this.view = new View();
  }

}

$(document).ready(function(){
  var app = new App();
});
