import {
  Vector3,
  MOUSE,
  Euler,
  Ray,
  Quaternion,
  Spherical,
  Matrix4,
  Vector2,
  Raycaster,
  OrthographicCamera,
  PerspectiveCamera,
  EventDispatcher
} from 'three';

import {TweenMax} from 'gsap';

import {Math as MMath} from 'three';

export default class Controls extends EventDispatcher {
  constructor(object, domElement,camera) {
    super();

    this.size = new Vector2();

    this.raycaster = new Raycaster();
    this.road = 0
    this.object = object;
    this.domElement = (domElement !== undefined) ? domElement : document;
    this.camera = camera;

    this.rotQuaternion = new Quaternion();
    // Set to false to disable this control
    this.enabled = true;

    // How far you can orbit vertically, upper and lower limits.
    // Range is 0 to Math.PI radians.
    this.minPitchAngle = 0; // radians
    this.maxPitchAngle = Math.PI; // radians

    // How far you can orbit horizontally, upper and lower limits.
    // If set, must be a sub-interval of the interval [ - Math.PI, Math.PI ].
    this.minYawAngle = -Infinity; // radians
    this.maxYawAngle = Infinity; // radians

    this.rotateSpeed = 1.0;

    // Set to true to automatically rotate around the target
    // If auto-rotate is enabled, you must call controls.update() in your animation loop
    this.autoRotate = false;
    this.autoRotateSpeed = 2.0; // 30 seconds per round when fps is 60

    // Mouse buttons
    this.mouseButtons = {
      ORBIT: MOUSE.LEFT,
    };

    this.changeEvent = {type: 'change'};
    this.startEvent = {type: 'start'};
    this.endEvent = {type: 'end'};

    this.STATE = {
      NONE: -1,
      ROTATE: 0,
      DOLLY: 1,
      PAN: 2,
      TOUCH_ROTATE: 3,
      TOUCH_DOLLY: 4,
      TOUCH_PAN: 5
    };

    this.state = this.STATE.NONE;

    this.EPS = 0.000001;

    this.pitch = 0
    this.yaw = 0

    this.animPitch = this.animYaw = 0

    this.rotateStart = new Vector2();
    this.rotateEnd = new Vector2();
    this.rotateDelta = new Vector2();

    this.downMBind = this.onMouseDown.bind(this)
    this.moveMBind =  this.onMouseMove.bind(this)
    this.upMBind = this.onMouseUp.bind(this)
    this.startTBind = this.onTouchStart.bind(this)
    this.endTBind = this.onTouchEnd.bind(this)
    this.moveTBind = this.onTouchMove.bind(this)
    this.cursorTBind = this.onMouseOver.bind(this)


    this.domElement.addEventListener('mousedown', this.downMBind, false);
    this.domElement.addEventListener('mousemove', this.cursorTBind, false);

    this.domElement.addEventListener('touchstart', this.startTBind, false);
    this.domElement.addEventListener('touchend', this.endTBind, false);
    this.domElement.addEventListener('touchmove', this.moveTBind, false);

    // force an update at start

    this.update();
  };

  getPolarAngle() {
      return this.spherical.phi;
  };

  getAzimuthalAngle() {
      return this.spherical.theta;
  };

  reset() {
    this.object.position.copy(this.position0);
    this.object.updateProjectionMatrix();
    this.dispatchEvent(this.changeEvent);
    this.update();
    this.state = this.STATE.NONE;
  };

  // this method is exposed, but perhaps it would be better if we can make it private...
  update() {
    this.rotQuaternion =new Quaternion()
    this.rotQuaternion.setFromEuler(new Euler(this.animPitch,
                                              0,//MMath.degToRad(this.rotateDelta.x * this.rotateSpeed),
                                              0,
                                              'XYZ')
                                   );

    this.object.quaternion.copy(new Quaternion())
    this.object.quaternion.multiplyQuaternions(this.rotQuaternion, this.object.quaternion);
    this.object.rotateOnAxis(new Vector3(0,1,0),this.animYaw)
  };

  getAutoRotationAngle() {
    return 2 * Math.PI / 60 / 60 * this.autoRotateSpeed;
  }

  //
  // event callbacks - update the object state
  //

  handleMouseDownRotate(event) {
    this.road = 0
    this.rotateStart.set(event.clientX, event.clientY);
  }

  handleMouseMoveRotate(event) {
    this.rotateEnd.set(event.clientX, event.clientY);
    this.rotateDelta.subVectors(this.rotateEnd, this.rotateStart);

    this.road += this.rotateDelta.length()

    this.rotateStart.copy(this.rotateEnd);

    this.rotate()
  }

  rotate(){
    this.pitch += MMath.degToRad(this.rotateDelta.y * this.rotateSpeed)
    this.yaw += MMath.degToRad(this.rotateDelta.x * this.rotateSpeed)

    this.pitch = Math.max( this.minPitchAngle, Math.min(this.maxPitchAngle, this.pitch) );
    this.yaw = Math.max( this.minYawAngle, Math.min(this.maxYawAngle, this.yaw) );

    this.animate();
  }

  animate(){
    TweenMax.to(this,2,{animPitch:this.pitch,animYaw:this.yaw,onUpdate:this.update,onUpdateScope:this,ease:Expo.easeOut})
  }

  handleInteraction(event,type){

    var mouse = new Vector2()
    var rect = this.domElement.getBoundingClientRect();
    mouse.x = ( ( event.clientX- rect.left ) / ( rect.right - rect.left ) ) * 2 - 1;
    mouse.y = - ( ( event.clientY - rect.top ) / ( rect.bottom - rect.top) ) * 2 + 1;


    this.raycaster.setFromCamera( mouse, this.camera );


    var intersects = this.raycaster.intersectObject(this.object,true);

    this.domElement.style.cursor = intersects.length > 0?"pointer":"auto";

    if(this.road > 20 || type) return;


    if(intersects.length >0){
      this.object.worldToLocal(intersects[0].point)
      this.dispatchEvent( {type: 'select',point:intersects[0].point});
      // intersects[i].object.parent.parent.select(intersects[0].object.parent.num);
    }
  }

  handleMouseUp(event) {
    this.handleInteraction(event,0);
    // console.log( 'handleMouseUp' );
  }

  handleTouchStartRotate(event) {
    this.road = 0;
    this.rotateStart.set(event.touches[0].pageX, event.touches[0].pageY);
  }
  handleTouchMoveRotate(event) {

    this.rotateEnd.set(event.touches[0].pageX, event.touches[0].pageY);
    this.rotateDelta.subVectors(this.rotateEnd, this.rotateStart);

    this.road += this.rotateDelta.length()
    this.rotateStart.copy(this.rotateEnd);

    this.rotate()
  }

  handleTouchEnd(event) {
    this.handleInteraction(event,0);
    //console.log( 'handleTouchEnd' );
  }
  //
  // event handlers - FSM: listen for events and reset state
  //

  onMouseOver(event){
    this.handleInteraction(event,1);
  }

  onMouseDown(event) {
    if (this.enabled === false) return;
    event.preventDefault();
    if (event.button === this.mouseButtons.ORBIT) {
      if (this.enableRotate === false) return;
      this.handleMouseDownRotate(event);
      this.state = this.STATE.ROTATE;
    }
    if (this.state !== this.STATE.NONE) {

      document.addEventListener('mousemove', this.moveMBind, false);
      document.addEventListener('mouseup', this.upMBind, false);

      this.dispatchEvent(this.startEvent);
    }
  }

  onMouseMove(event) {
    if (this.enabled === false) return;
    event.preventDefault();
    if (this.state === this.STATE.ROTATE) {
      if (this.enableRotate === false) return;
      this.handleMouseMoveRotate(event);
    }
  }

  onMouseUp(event) {
    if (this.enabled === false) return;

    this.handleMouseUp(event);

    document.removeEventListener('mousemove', this.moveMBind, false);
    document.removeEventListener('mouseup', this.upMBind, false);
    this.dispatchEvent(this.endEvent);
    this.state = this.STATE.NONE;
  }

  onTouchStart(event) {
    if (this.enabled === false) return;

    switch (event.touches.length) {
      case 1: // one-fingered touch: rotate
          if (this.enableRotate === false) return;
          this.handleTouchStartRotate(event);
          this.state = this.STATE.TOUCH_ROTATE;
        break;
      default:
        this.state = this.STATE.NONE;
    }

    if (this.state !== this.STATE.NONE) {
      this.dispatchEvent(startEvent);
    }
  }

  onTouchMove(event) {

    if (this.enabled === false) return;

    event.preventDefault();
    event.stopPropagation();

    switch (event.touches.length) {
      case 1: // one-fingered touch: rotate
          if (this.enableRotate === false) return;
          if (this.state !== this.STATE.TOUCH_ROTATE) return; // is this needed?...
          this.handleTouchMoveRotate(event);
        break;
      default:
          this.state = this.STATE.NONE;
    }
  }

  onTouchEnd(event) {
    if (this.enabled === false) return;
    this.handleTouchEnd(event);
    this.dispatchEvent(this.endEvent);
    this.state = this.STATE.NONE;
  }

  setSize(w,h){
    this.size.set(w,h) ;
  }

  dispose() {
    this.domElement.removeEventListener('mousedown', this.moveMBind, false);
    document.removeEventListener('mousemove', this.moveMBind, false);
    document.removeEventListener('mouseup', this.upMBind, false);
    this.domElement.removeEventListener('touchstart', this.startTBind, false);
    this.domElement.removeEventListener('touchend', this.endTBind, false);
    this.domElement.removeEventListener('touchmove', this.moveTBind, false);
  };
}
