var config = require('../../config')
if(!config.tasks.production.rev) return

var gulp         = require('gulp')
var gutil        = require('gulp-util')
var gulpSequence = require('gulp-sequence')

// If you are familiar with Rails, this task the equivalent of `rake assets:precompile`
var revTask = function(cb) {
  gulpSequence(
    'update-html',
  cb)
}

gulp.task('rev', revTask)
module.exports = revTask
