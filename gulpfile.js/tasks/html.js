var config       = require('../config');
if(!config.tasks.html) return

var browserSync  = require('browser-sync');
var data         = require('gulp-data');
var gulp         = require('gulp');
var gulpif       = require('gulp-if');
var handleErrors = require('../lib/handleErrors');
var htmlmin      = require('gulp-htmlmin');
var path         = require('path');
var render       = require('gulp-nunjucks-render');
var fs           = require('fs');
var replace      = require('gulp-replace');
var twig = require('gulp-twig');

var exclude = path.normalize('!**/{' + config.tasks.html.excludeFolders.join(',') + '}/**');

var paths = {
  src: [path.join(config.root.src, config.tasks.html.src, '/*.{' + config.tasks.html.extensions + '}'), exclude],
  dest: path.join('./')
}

var getData = function(file) {
  var dataPath = path.resolve(config.root.src, config.tasks.html.src, config.tasks.html.data)
  var fileName = dataPath + '/' + path.basename(file.path, '.tpl') + '.json';

  if (fs.existsSync(fileName)) {
     return JSON.parse(fs.readFileSync(fileName, 'utf8'));
  }
}

var getDataOne = function(file) {
  var dataPath = path.resolve(config.root.src, config.tasks.html.src, config.tasks.html.dataFile)
  return JSON.parse(fs.readFileSync(dataPath, 'utf8'))
}

var manageEnvironment = function(environment) {
  environment.addFilter('split', function(str, seperator) {
      return str.split(seperator);
  });

  environment.addFilter('pronouns', function(str) {
      var arr = ['a', 'i', 'w', 'o', 'z', 'A', 'I', 'W', 'O'];

      if (str !== undefined) {
        for (i = 0; i<= arr.length; i = i + 1) {
          str = str.replace(' ' + arr[i] + ' ', ' ' + arr[i] + '&nbsp;');
        }
      }

      return str;
  });

  environment.addFilter('sanitize', function(str) {
      var arr = {'ś': 's', 'ż': 'z', 'ą': 'a', "ł": 'l', "ó": 'o', 'ź': 'z', 'ę': 'e', 'ć': 'c'};

      if (str !== undefined) {
        str = str.toLowerCase();
        str = str.replace(/,/g,'_');
        str = str.replace(/ /g,'_');
        var arr = Object.keys(arr).map(function (key) {
          var k = new RegExp(key, 'g');
          str = str.replace(k, arr[key]);
        });
      }

      return str;
  });
}

var htmlTask = function() {
  return gulp.src(paths.src)
    .pipe(data(getData))
    .pipe(data(getDataOne))
    .on('error', handleErrors)
    .pipe(twig({
      filters: [
        {
          name: "pronouns",
          func: function (str) {
            var arr = ['a', 'i', 'w', 'o', 'z', 'A', 'I', 'W', 'O'];

            if (str !== undefined) {
              for (i = 0; i<= arr.length; i = i + 1) {
                str = str.replace(' ' + arr[i] + ' ', ' ' + arr[i] + '&nbsp;');
              }
            }

            return str;
          }
        },
        {
          name: "safe",
          func: function (str) {
              return str;
          }
        },
        {
          name: "sanitize",
          func: function (str) {
            var arr = {'ś': 's', 'ż': 'z', 'ą': 'a', "ł": 'l', "ó": 'o', 'ź': 'z', 'ę': 'e', 'ć': 'c'};

            if (str !== undefined) {
              str = str.toLowerCase();
              str = str.replace(/,/g,'_');
              str = str.replace(/ /g,'_');
              var arr = Object.keys(arr).map(function (key) {
                var k = new RegExp(key, 'g');
                str = str.replace(k, arr[key]);
              });
            }

            return str;
          }
        }
      ]
    }))
/*    .pipe(render({
      path: [path.join(config.root.src, config.tasks.html.src)],
      envOptions: {
        watch: false,
        autoescape: false
      },
      manageEnv: manageEnvironment
    }))*/
    .on('error', handleErrors)
    .pipe(gulpif(global.production, replace(' <br', '&nbsp;<br')))
    .pipe(gulpif(global.production, replace('files/', '../files/')))
    //.pipe(gulpif(global.production, htmlmin(config.tasks.html.htmlmin)))
    .pipe(gulp.dest(path.join(global.production ? config.root.dist : '', paths.dest)))
    .pipe(gulpif(!global.production, browserSync.stream()))
}

gulp.task('html', htmlTask);
module.exports = htmlTask;
