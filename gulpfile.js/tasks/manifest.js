var config = require('../config')
var gulp = require('gulp')
var merge = require('gulp-merge-json')

gulp.task('merge-json', function () {
  gulp.src(config.mergeJsons)
        .pipe(merge({
          fileName: 'manifest.json'
        }))
        .pipe(gulp.dest('./dist/tpl'))
})
